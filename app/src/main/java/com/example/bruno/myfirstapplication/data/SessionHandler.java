package com.example.bruno.myfirstapplication.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.bruno.myfirstapplication.R;
import com.example.bruno.myfirstapplication.model.User;

/**
 * Created by bruno on 27/01/18.
 */

public class SessionHandler {

    private static final String GROUP_USER = "USER_DATA";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_TOKEN = "token";
    private static final String FIELD_PHOTO_URL = "photo_url";
    private static final String EMPTY_STRING = "";

    public void saveSession(User user, Context context) {
        SharedPreferences preferences = context.getSharedPreferences(GROUP_USER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(FIELD_NAME, user.getName());
        editor.putString(FIELD_TOKEN, user.getToken());
        editor.putString(FIELD_PHOTO_URL, user.getPhotoUrl());
        editor.commit();
    }


    public User getUser(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(GROUP_USER, Context.MODE_PRIVATE);

        final String nome = preferences.getString(FIELD_NAME, EMPTY_STRING);
        final String token = preferences.getString(FIELD_TOKEN, EMPTY_STRING);
        final String photoUrl = preferences.getString(FIELD_PHOTO_URL, EMPTY_STRING);

        if(EMPTY_STRING.equals(nome) || EMPTY_STRING.equals(token)) {
            throw new RuntimeException(context.getString(R.string.error_no_user));
        }

        return new User(nome, token, photoUrl);
    }

    public void removeSession(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(GROUP_USER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.clear();
        editor.commit();
    }
}
