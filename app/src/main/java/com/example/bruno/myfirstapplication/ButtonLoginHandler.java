package com.example.bruno.myfirstapplication;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.example.bruno.myfirstapplication.web.WebTaskLogin;

/**
 * Created by bruno on 27/01/18.
 */

public class ButtonLoginHandler implements View.OnClickListener {

    private static final String EMPTY_STRING = "";
    private static final String TESTE = "teste";
    private Activity activity;

    public ButtonLoginHandler(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void onClick(View view) {
        hideKeyboard();

        final String email = validateTextField(view, R.id.name, R.string.field_email_empty);
        if(email == null) return;

        final String senha = validateTextField(view, R.id.senha, R.string.field_password_empty);
        if (senha == null) return;

        Log.d("LIFECICLE", String.format("Usuario: %s / Senha: %s", email, senha));

        WebTaskLogin taskLogin = new WebTaskLogin(activity, email, senha);
        taskLogin.execute();
    }

    @Nullable
    private String validateTextField(View view, int campoSenha, int mensagemErro) {
        final TextView passwordTextView = activity.findViewById(campoSenha);
        final String senha = passwordTextView.getText().toString();
        if (EMPTY_STRING.equals(senha)) {
            Snackbar.make(view, activity.getString(mensagemErro), Snackbar.LENGTH_LONG).show();
            return null;
        }
        return senha;
    }

    public void hideKeyboard(){
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm =
                    (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }



}
