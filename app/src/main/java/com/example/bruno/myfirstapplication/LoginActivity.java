package com.example.bruno.myfirstapplication;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.example.bruno.myfirstapplication.data.SessionHandler;
import com.example.bruno.myfirstapplication.model.User;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v("LIFECICLE", "Criando...");
        setContentView(R.layout.activity_login);

        Button loginButton = findViewById(R.id.button_login);
        loginButton.setOnClickListener(new ButtonLoginHandler(this));
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.v("LIFECICLE", "Iniciando..");
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.v("LIFECICLE", "Resumindo...");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v("LIFECICLE", "Pausando...");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.v("LIFECICLE", "Parando...");
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.v("LIFECICLE", "Reiniciando...");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.v("LIFECICLE", "Morrendo...");
    }

    @Subscribe
    public void onError(Error error){
        failedLogin(error.getMessage());
    }

    @Subscribe
    public void onUser(User user){
        SessionHandler sessionHandler = new SessionHandler();
        sessionHandler.saveSession(user,this);
        loginComSucesso();
    }


    private void failedLogin(String errorMessage){
        Snackbar
                .make(getCurrentFocus(), errorMessage, Snackbar.LENGTH_LONG)
                .show();
    }

    private void loginComSucesso() {
        finish();
    }



}
