package com.example.bruno.myfirstapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.example.bruno.myfirstapplication.data.SessionHandler;
import com.example.bruno.myfirstapplication.model.User;
import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity {

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onStart() {
        super.onStart();

        SessionHandler sessionHandler = new SessionHandler();

        try {
            user = sessionHandler.getUser(this);
            getSupportActionBar().setTitle(user.getName());
            ImageView imgProfile = findViewById(R.id.img_profile);
            Picasso.with(this).load(user.getPhotoUrl()).into(imgProfile);
        } catch (RuntimeException x){
            sessionHandler.removeSession(this);
            Intent myIntent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(myIntent);
        }
    }
}
