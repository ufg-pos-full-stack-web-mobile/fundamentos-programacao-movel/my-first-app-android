package com.example.bruno.myfirstapplication.web;

import android.content.Context;

import com.example.bruno.myfirstapplication.R;
import com.example.bruno.myfirstapplication.model.User;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by bruno on 27/01/18.
 */

public class WebTaskLogin extends WebTaskBase {

    private static final String SERVICE_URL = "login";
    private final String username;
    private final String password;

    public WebTaskLogin(Context context, String username, String password) {
        super(context, SERVICE_URL);
        this.username = username;
        this.password = password;
    }

    @Override
    void handleResponse(String response) {
        try {
            String username = new JSONObject(response).getString("nome");
            String token = new JSONObject(response).getString("token");
            String photoUrl = new JSONObject(response).getString("photoUrl");

            User user = new User();
            user.setName(username);
            user.setToken(token);
            user.setPhotoUrl(photoUrl);

            EventBus.getDefault().post(user);

        } catch (JSONException e) {
            EventBus.getDefault().post(new Error(getContext().getString(R.string.error_field_username)));
        }
    }

    @Override
    String getRequestBody() {
        Map<String, String> map = new HashMap<>();
        map.put("username", username);
        map.put("token", password);

        JSONObject json = new JSONObject(map);
        return json.toString();
    }
}
