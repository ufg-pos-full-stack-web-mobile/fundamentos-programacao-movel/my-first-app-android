package com.example.bruno.myfirstapplication.model;

/**
 * Created by bruno on 27/01/18.
 */

public class User {

    private String name;
    private String token;
    private String photoUrl;

    public User() {
    }

    public User(String nome, String token, String photoUrl) {
        this.name = nome;
        this.token = token;
        this.photoUrl = photoUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
